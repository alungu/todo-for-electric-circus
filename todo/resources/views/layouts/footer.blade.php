<div class="large-12 medium-12 cell columns">
    <div class="grid-x grid-padding-x">
        <div class="large-12 medium-12 columns cell footer">
            <p> Click on the content of an item to edit it. Only incomplete todos can be edited.</p>
            <p> Created by : Alex Lungu <a href="mailto:a.lungu@hotmail.com">a.lungu@hotmail.com</a></p>
        </div>
    </div>
</div>
