<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo App</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/app.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        window.Laravel = { csrfToken: '{{ csrf_token() }}' }
    </script>
  </head>
  <body>
        @yield('content')

        @include('layouts.footer')

        <script src="/js/app.js"></script>
        <script>

            $(document).foundation();

        </script>
  </body>
</html>
