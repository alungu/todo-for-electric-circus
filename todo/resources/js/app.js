require('./bootstrap');

import axios from 'axios';

window.Vue = require('vue');
window.bus = new Vue();
window.axios = axios;

import TodoList from './components/TodoList.vue';

const app = new Vue({
    render: h => h(TodoList)
}).$mount('#app');
