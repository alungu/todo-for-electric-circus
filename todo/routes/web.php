<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('todos', 'App\Http\Controllers\TodoController@index');
Route::post('create_todo', 'App\Http\Controllers\TodoController@store');
Route::post('edit_todo/{id}', 'App\Http\Controllers\TodoController@edit');
Route::post('delete_todo/{id}', 'App\Http\Controllers\TodoController@destroy');
Route::post('complete_todo/{id}', 'App\Http\Controllers\TodoController@complete');
