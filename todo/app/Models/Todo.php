<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\FullTextSearch;

class Todo extends Model
{
    use HasFactory, FullTextSearch;

    protected $fillable = [
        'content',
        'completed',
    ];

    protected $casts = [
        'completed' => 'boolean',
    ];

    public function scopeWhenSearch($query, $search)
    {
        return $query->when($search, function ($q) use ($search) {
            return $q->whereRaw("MATCH (content) AGAINST (? IN BOOLEAN MODE)" , $this->tokenizeForFullTextSearch($search));
        });
    }

}
