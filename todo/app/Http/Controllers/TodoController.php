<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            return Todo::whenSearch(request()->search)->get();
        }

        return Todo::orderBy('updated_at', 'desc')->get();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'content' => 'required|max:500'
        ]);

        return Todo::create(['content' => request('content')]);
    }

    public function edit(Request $request)
    {
        $update = $this->validate($request, [
            'content' => 'required|max:500'
        ]);

        $todo = Todo::findOrFail($request->id);
        $todo->content = $request->content;

        $todo->save();

        return response($todo, 200);
    }

    public function complete($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->completed = !$todo->completed;
        $todo->save();

        return response($todo, 200);
    }

    public function destroy($id)
    {
        $task = Todo::findOrFail($id);
        $task->delete();
    }
}
