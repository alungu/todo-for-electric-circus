## Intro

Build a demo todo single page application using Foundation for Sites 6.6.3 pwd by VueJS 2.6, Laravel 8 and Mysql 5.7..

## Screenshots

![Loading Screen](/Screenshots/preload.png "Loading Sreen")


![Todo List](/Screenshots/list.png "To do list")


![Editing todo](/Screenshots/edit.png "Editing to do")

![Database structure](/Screenshots/db_struct.png "Database structure")

![Database Indexes](/Screenshots/db_indexes.png "Database indexes")

## Infrastructure
This proof of concept runs on 3 docker containers
* Web container running nginx with node (node:14.2-alpine && nginx:1.18-alpine) as
* App container running php 7.3 from php:7.3-fpm. In hindsight I should have probably tried out php 8
* Db container running mysql 5.7 from mysql:5.7. Again this should have been mysql 8 but SequelPro did not want to connect to the host of 127.0.0.1 even with the correct port exposed inside docker.

## Prerequisites

In order to deploy this webapp, the host machine should have
[Docker and Docker compose](https://docs.docker.com/compose/) installed.

## How to

```bash
$ git clone https://gitlab.com/alungu/todo-for-electric-circus.git todo-app
$ cd todo-app
$ make init
```

Then head over to http://localhost

This repo also includes a useful [Makefile](https://gitlab.com/alungu/todo-for-electric-circus/blob/master/Makefile) I tend to include with all projects to help with various commands that may be required on deployment.


## Long Grass / Ideas
* Add user registration and authentication routes to allow todos for multiple users
* Add filtering by todo completed flag (db indexes in place for that)
* Add searching by content (db fulltext index added + scopeSearch defined)
* Pin todos based on user preferences
* Unit Tests!!!
